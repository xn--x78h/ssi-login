---
---
<meta charset="utf8">
## SSI keys infrastructure


For the system only publickeys are identity identifiers.<br>
For the local user, publickeys can received "petnames" or "globally recognized name".

There is no registry of accounts, each user is responsible of its own key pairs.

1. user login w/ petname and pin
2. an unwrapping key (wku) is derived from a device salt
3. a device private key is decrypted (skd)
4. the device private key is used to compute the broker shared secret
5. the broker shared secret is mixed with an access key (xku) to reveal
   the user private key (sku)
6. the user private key is used to sign the user public key
7. the signature is packaged into a JSON web-token for authentication

![ssi-key graph](ssikeys.svg)
