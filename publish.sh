#

find . -name '*.~1' -delete
tic=$(date +%s%N | cut -c-13)
export JEKYLL_ENV=production
jekyll build 1> /dev/null
qm=$(ipfs add -r _site -Q)
echo $tic: $qm >> qm.log

ipfs pin remote add --service=phenomx --name=ssilogin $qm
