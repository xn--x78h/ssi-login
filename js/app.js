

(() => {
var BASES = {
   radix16: '0123456789ABCDEF',
   radix26: 'ABCDEFGHiJKLMNoPQRSTUVWXYZ',
   radix36: 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789',
   radix43: 'ABCDEFGHiJKLMNoPQRSTUVWXYZ0123456789 -+.$%*',
   base58: '.123456789ABCDEFGH.JKLMN.PQRSTUVWXYZabcdefghijk.mnopqrstuvwxyz'.replace(/\./g,''), // ![0IOl]
   base58f: '.123456789abcdefghijk.mnopqrstuvwxyzABCDEFGH.JKLMN.PQRSTUVWXYZ'.replace(/\./g,''), // ![0IOl]
   base85: '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'+'!#$%&()*+-;<=>?@^_`{|}~',
}

var reset = true;
var seedBuf = new ArrayBuffer(32);
var seedView = new Uint8Array(seedBuf);
var devid = localStorage.getItem('devid')
if (reset || devid === null || devid == '' || 'undefined' == typeof(devid)) {
 console.log('// create a random devid');
 // create a random devid
 window.crypto.getRandomValues(seedView);
 console.log('seedBuf:',seedBuf);
 console.log('seedView16:',ab2hex(seedView,':'));
 let devid58 = encode_base(seedView,'base58f');
 console.log('devid58:',devid58);
 devid = ab2str(seedBuf);
 
 localStorage.setItem('devid',devid)

} else {
  console.log('// re-use devid');
  seedBuf = str2ab(devid);
  console.log('seedBuf:',seedBuf);
  let seedView = new Uint8Array(seedBuf);
  console.log('localStorage.devid:',ab2hex(seedView,':'));
  devid = ab2str(seedBuf);
  let devid58 = encode_base(seedView,'base58f');
  /* decode_base(devid58,'base58f'); */
  console.log('devid16:',encode_base(seedView,'radix16'));
  console.log('devid58:',devid58);

}

let keyMaterial = getKeyMaterial();
let salt = window.crypto.getRandomValues(new Uint8Array(16));
let iv = window.crypto.getRandomValues(new Uint8Array(16));
console.log('keyMaterial:',keyMaterial);
console.log('iv:',iv);
var key = getKey(keyMaterial,salt,iv);
console.log('key:',key);
let algorithm = { "name": "AES-CTR", "counter": iv, "length": 128};
var cipher = encrypt(seedBuf, key, iv);
console.log('cipher:',cipher);


async function encrypt(plaintext, key, iv) {
  return window.crypto.subtle.encrypt(
     { "name": "AES-CTR", "counter": iv, "length": 128},
     key,
     plaintext
  )
}

function getWrappingKey(keyMaterial, salt) { /* no iv necessary */
    return window.crypto.subtle.deriveKey(
      {
        "name": "PBKDF2",
        salt: salt, 
        "iterations": 100000,
        "hash": "SHA-256"
      },
      keyMaterial,
      { "name": "AES-KW", "length": 256},
      true,
      [ "wrapKey", "unwrapKey" ]
    )
}
async function wrapCryptoKey(keyToWrap) {
    // get the key encryption key
    const keyMaterial = await getKeyMaterial();
    salt = window.crypto.getRandomValues(new Uint8Array(16));
    const wrappingKey = await getWrappingKey(keyMaterial, salt);

    const wrapped = await window.crypto.subtle.wrapKey(
      "raw",
      keyToWrap,
      wrappingKey,
      "AES-KW"
    );
    const wrappedKeyBuffer = new Uint8Array(wrapped);
    console.log('wrappedKey:',`[${wrappedKeyBuffer}]`);
    return wrappedKeyBuffer;
}
async function unwrapCryptoKey(keyToUnwrap) {
    // get the key encryption key
    const keyMaterial = await getKeyMaterial();
    const salt = bytesToArrayBuffer(saltBytes);
    const wrappingKey = await getWrappingKey(keyMaterial, salt);

    const wrapped = await window.crypto.subtle.wrapKey(
      "raw",
      keyToWrap,
      wrappingKey,
      "AES-KW"
    );
    const wrappedKeyBuffer = new Uint8Array(wrapped);
    console.log('wrappedKey:',`[${wrappedKeyBuffer}]`);
    return wrappedKeyBuffer;
}  


function getAESKey(keyMaterial, salt, iv) {
    return window.crypto.subtle.deriveKey(
      {
        "name": "PBKDF2",
        salt: salt, 
        "iterations": 100000,
        "hash": "SHA-256"
      },
      keyMaterial,
      { "name": "AES-CTR", "counter": iv, "length": 128},
      true,
      [ "encrypt", "decrypt" ]
    )
}
function getECKey(keyMaterial, salt, iv) {
    return window.crypto.subtle.deriveKey(
      {
        "name": "EC",
        salt: salt, 
        "iterations": 100000,
        "hash": "SHA-256"
      },
      keyMaterial,
      { "name": "AES-CTR", "counter": iv, "length": 128},
      true,
      [ "encrypt", "decrypt" ]
    )
}




  

// PEM encoded X.509 key
const publicKey = 
`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAunF5aDa6HCfLMMI/MZLT
5hDk304CU+ypFMFiBjowQdUMQKYHZ+fklB7GpLxCatxYJ/hZ7rjfHH3Klq20/Y1E
bYDRopyTSfkrTzPzwsX4Ur/l25CtdQldhHCTMgwf/Ev/buBNobfzdZE+Dhdv5lQw
KtjI43lDKvAi5kEet2TFwfJcJrBiRJeEcLfVgWTXGRQn7gngWKykUu5rS83eAU1x
H9FLojQfyia89/EykiOO7/3UWwd+MATZ9HLjSx2/Lf3g2jr81eifEmYDlri/OZp4
OhZu+0Bo1LXloCTe+vmIQ2YCX7EatUOuyQMt2Vwx4uV+d/A3DP6PtMGBKpF8St4i
GwIDAQAB
-----END PUBLIC KEY-----`;
var pku = importPublicKey(publicKey);
console.log('pku:',pku);

function getKeyMaterial() { /* get password */
  let password = window.prompt("Enter your password");
  let enc = new TextEncoder();
  return window.crypto.subtle.importKey(
    "raw",
    enc.encode(password),
    "PBKDF2",
    false,
    ["deriveBits", "deriveKey"]
  );
}



let el = document.querySelector('[type=submit]')
console.log('el:', el)
if (el !== null && typeof(el) != 'undefined') {
  el.innerText = 'Register'
}

el = document.querySelector('[type=password]');
if (el !== null) {
  document.querySelector('[type=password]').autocomplete = true;
}











async function importPublicKey(spkiPem) {       
    return await window.crypto.subtle.importKey(
        "spki",
        getSpkiDer(spkiPem),
        {
            name: "RSA-OAEP",
            hash: "SHA-256",
        },
        true,
        ["encrypt"]
    );
}
function getSpkiDer(spkiPem){
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    var pemContents = spkiPem.substring(pemHeader.length, spkiPem.length - pemFooter.length);
    var binaryDerString = window.atob(pemContents);
    return str2ab(binaryDerString); 
}


// https://stackoverflow.com/a/11058858
function str2ab(str) {
    const buf = new ArrayBuffer(str.length);
    const bufView = new Uint8Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}
    
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}
function ab2hex(buf,s) { /* Uint8Array to Hex */
    return Array.prototype.map.call(buf, x => ('0' + x.toString(16)).slice(-2)).join(s);
}



function alphabet(b) {
  let abet;
  if (b in BASES) {
    abet = BASES[b]
  } else {
    for (let base of Object.keys(BASES)) {
      if (b < BASES[base].length) { abet = BASES[base]; break; }
    }
  }
  //console.log('alphabet.abet:',abet)
  return abet;
}
function alphamap(b) {
  let amap = {};
  let abet = alphabet(b);
  for (i = 0; i < abet.length; i++) {
    amap[abet.charAt(i)] = i
  }
  return amap
}

function encode_base(buffer,base) {
  let v = 0;
  var carry, digits, j;
  if (buffer.length === 0) {
    return "";
  }
  var alphab = alphabet(base)
  console.log('encode.alphab[%s]: %s (#%s)',base,alphab,alphab.length);
  var N = alphab.length;
  digits = [0];
  for (i = 0; i < buffer.length ; i++) {
    for(j = 0; j < digits.length ; j++) {
      digits[j] <<= 8; // * 256
    }
    if (v) { console.log('* 256;  digits: [%s]',digits.join(',')) }
    digits[0] += buffer[i];
    if (v) { console.log('+ %s;  digits: [%s]',buffer[i],digits.join(',')) }
    carry = 0;
    for (j = 0; j < digits.length; j++) {
      digits[j] += carry;
      carry = (digits[j] / N) | 0; // bitwise or : make sure it is an integer
      digits[j] %= N;
    }
    if (v) { console.log('carry = x%s (%s)',carry.toString(16),carry) }
    while (carry) {
      digits.push(carry % N);
      carry = (carry / N) | 0;
    }
  }
  i = 0;
  /* wrong ! ... */
  while (buffer[i] === 0 && i < buffer.length - 1) {
    digits.push(0);
    i++;
  }
  return digits.reverse().map(function(digit) {
    return alphab[digit];
  }).join("");
};
function decode_base(string, base) {
  let v = 1;
  var bytes, c, carry, j;
  if (string.length === 0) {
    return new (typeof Uint8Array !== "undefined" && Uint8Array !== null ? Uint8Array : Buffer)(0);
  }
  var ab = alphabet(base);
  console.log('decode.alphab[%s]: %s (#%s)',base,ab,ab.length);
  var zero = ab[0]; // TODO : optimized alphabet+alphamap
  var one = ab.slice(-1);
  if (v) { console.log('zero: %s, one: %s',zero,one); }
  var map = alphamap(base);
  if (v) { console.log('decode.map:',map); }
  var N = Object.keys(map).length;
  if (v) { console.log('N:',N) }
  bytes = [0];
  weights = [0];

  for (i = 0;i < string.length; i++) {
    c = string[string.length -1 - i]; // consume string from the end
    c = string[i];
    if (!(c in map)) {
      throw "BaseN.decode received unacceptable input. Character '" + c + "' is not in the BaseN alphabet :"+ alphabet(base);
    }

    // shift left bytes
    if (v) { console.log("i:%s; bytes: [%s] c:%s m[%s]=d'%s=x%s",i,a2s(bytes,','),c,c,map[c],map[c].toString(16)) }
    for (j = 0; j < bytes.length; j++) {
      bytes[j] *= N;
      weights[j] *= N;
    }
    if (v) { console.log('* N=%s;  bytes: [%s]',N,a2s(bytes,',')) }
    bytes[0] += map[c]; // add current chars' value
    if (v) { console.log('+ map[c];  bytes: [%s] c:%s map[%s]=x%s',a2s(bytes,','),c,c,map[c].toString(16)) }

    weights[0] += N-1;
    let pos = 0; // compute weight ...
    carry = 0; // compute carry ...
    for (j = 0; j < bytes.length; j++) {
      weights[j] += pos;
      pos = weights[j] >> 8;
      weights[j] &= 0xff;

      bytes[j] += carry;
      carry = bytes[j] >> 8;
      bytes[j] &= 0xff;
    }
    if (v) { console.log('        ;  weights: [%s]',a2s(weights,',')) }
    if (v) { console.log('carry = 0x%s (%s)',carry.toString(16),carry) }
    while (pos) { // propagate carry
      if (carry) {
        bytes.push(carry & 0xff);
        carry >>= 8;
        if (v) { console.log(' reminder: carry: 0x%s',carry.toString(16)) }
      } else {
        bytes.push(0);
      }
      weights.push(pos & 0xff);
      pos >>= 8;
      if (v) { console.log(' push: bytes: [%s]',a2s(bytes,',')) }
      if (v) { console.log('     weights: [%s]',a2s(weights,'.')) }
    }

  }
  if (v) { console.log('bytes: [%s]',a2s(bytes,',')); }
  /*
          for(i = 0; string[i] === zero && i < string.length - 1; i++) {
          console.log('adding some zeros ... string[%s]=%s',i,string[i])
          bytes.push(0);
          }
          */
  return new (typeof Uint8Array !== "undefined" && Uint8Array !== null ? Uint8Array : Buffer)(bytes.reverse());
};

function a2s(array,c) {
    return Array.prototype.map.call(array, x => 'x'+('0' + x.toString(16)).slice(-2)).join(c);
}
/*
Convert  an ArrayBuffer into a string
from https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
*/
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function bytes2ab(bytes) {
  const bytesAsArrayBuffer = new ArrayBuffer(bytes.length);
  const bytesUint8 = new Uint8Array(bytesAsArrayBuffer);
  bytesUint8.set(bytes);
  return bytesAsArrayBuffer;
}


})();
